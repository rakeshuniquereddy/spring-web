package com.oehm3.webdemo;

import org.springframework.stereotype.Component;

@Component
public class CommonController {

	public CommonController() {
		System.out.println(this.getClass().getSimpleName() +" created");
	}

}
